import uno

class SheetDocument(object):
    def __init__(self, doc):
        self.doc = doc
        
    def __getitem__(self, index):
        retriever = self.doc.getSheets()
        try:
            index = int(index)
            method = retriever.getByIndex
        except ValueError:
            method = retriever.getByName
        sheet = method(index)
        return Sheet(sheet)
        
    def __repr__(self):
        return "Spreadsheet  %s " % self.doc.getTitle()


class Sheet(object):
    def __init__(self, sheet):
        self.sheet = sheet
    
    def _getcell(self, index):
        if isinstance(index, str):
            letters = "".join(letter for letter in index if letter.isalpha()).upper()
            row = int("".join(letter for letter in index if letter.isdigit())) - 1
            col = 0
            for letter in letters:
                col *= 26
                col += ord(letter) - ord("@")
            col -= 1
            index = (col, row)
        return self.sheet.getCellByPosition(*index)
    
    def __getitem__(self, index):
        return Cell(self._getcell(index))
    
    def __setitem__(self, index, value):
        cell = self._getcell(index)
        cell.setFormula(value)
    
    def __repr__(self):
        return self.sheet.getName()

class Cell(object):
    def __init__(self, cell):
        self.cell = cell

    def __repr__(self):
        return self.cell.getString().encode("utf-8")

    @property
    def color(self):
        color = self.cell.CellBackColor
        if color == -1:
            return None
        return (color >> 16, (color >> 8) & 0xff, color & 0xff)

    @color.setter
    def color(self, color):
        if color in (None, -1):
            color = -1
        else:
            color = (color[0] << 16) + (color[1] << 8) + color[2]
        self.cell.CellBackColor = color


def connect(port=2002):
    lctx = uno.getComponentContext()
    res = lctx.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", lctx)
    ctx = res.resolve("uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext")
    serv_man = ctx.ServiceManager
    desktop = serv_man.createInstanceWithContext("com.sun.star.frame.Desktop", ctx)
    return desktop

def loadSheet(filename):
    desktop = connect()
    doc = desktop.loadComponentFromURL("file://%s" % filename, "_blank", 0, ())
    return SheetDocument(doc)
